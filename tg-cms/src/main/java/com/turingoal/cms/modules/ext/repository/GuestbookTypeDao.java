package com.turingoal.cms.modules.ext.repository;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.turingoal.cms.modules.ext.domain.GuestbookType;
import com.turingoal.cms.modules.ext.domain.form.GuestbookTypeForm;
import com.turingoal.cms.modules.ext.domain.query.GuestbookTypeQuery;

/**
 * 留言板类型Dao
 */
public interface GuestbookTypeDao {

	/**
	 * 查询 留言板类型
	 */
	List<GuestbookType> find(final GuestbookTypeQuery query);

	/**
	 * 通过id得到一个 留言板类型
	 */
	GuestbookType get(final String id);

	/**
	 * 新增 留言板类型
	 */
	void add(final GuestbookTypeForm form);

	/**
	 * 修改 留言板类型
	 */
	int update(final GuestbookTypeForm form);

	/**
	 * 根据id删除一个 留言板类型
	 */
	int delete(final String id);

	/**
	 * 得到最大优先级
	 */
	int getMaxPriority(final GuestbookTypeForm form);

	/**
	 * 修改优先级
	 */
	int changePriority(final GuestbookTypeForm form);

	/**
	 * 递增优先级
	 */
	int increasePrioritys(final GuestbookTypeForm form);

	/**
	 * 递减优先级
	 */
	int decreasePrioritys(final GuestbookTypeForm form);

	/**
	 * 删除数据，更新其它数据优先级
	 */
	int changePrioritysByDelete(@Param("id") final String id);
}
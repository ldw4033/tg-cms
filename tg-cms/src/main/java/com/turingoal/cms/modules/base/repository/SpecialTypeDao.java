package com.turingoal.cms.modules.base.repository;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.turingoal.cms.modules.base.domain.SpecialType;
import com.turingoal.cms.modules.base.domain.form.SpecialTypeForm;
import com.turingoal.cms.modules.base.domain.query.SpecialTypeQuery;

/**
 * SpecialTypeDao
 */
public interface SpecialTypeDao {

	/**
	 * 查询 SpecialType
	 */
	List<SpecialType> find(final SpecialTypeQuery query);

	/**
	 * 通过id得到一个 SpecialType
	 */
	SpecialType get(final String id);

	/**
	 * 新增 SpecialType
	 */
	void add(final SpecialTypeForm form);

	/**
	 * 修改 SpecialType
	 */
	int update(final SpecialTypeForm form);

	/**
	 * 根据id删除一个 SpecialType
	 */
	int delete(final String id);

	/**
	 * 修改状态
	 */
	int changeState(final Map<String, Object> map);

	/**
	 * 得到最大优先级
	 */
	int getMaxPriority(final SpecialTypeForm form);

	/**
	 * 修改优先级
	 */
	int changePriority(final SpecialTypeForm form);

	/**
	 * 递增优先级
	 */
	int increasePrioritys(final SpecialTypeForm form);

	/**
	 * 递减优先级
	 */
	int decreasePrioritys(final SpecialTypeForm form);

	/**
	 * 删除数据，更新其它数据优先级
	 */
	int changePrioritysByDelete(@Param("id") final String id);

}
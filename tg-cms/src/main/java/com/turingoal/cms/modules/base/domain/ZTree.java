package com.turingoal.cms.modules.base.domain;

import java.util.List;
import lombok.Data;
/**
 * ztree 格式数据
 * @author Administrator
 *
 */
@Data
public class ZTree {

	private String id;
	private String pId;
	private String name;
	private Boolean checked;
	private Boolean open;
	private List<ZTree> children;
	
}

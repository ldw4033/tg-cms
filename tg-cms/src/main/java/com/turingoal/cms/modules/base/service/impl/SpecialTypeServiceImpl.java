package com.turingoal.cms.modules.base.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.turingoal.cms.core.commons.SystemHelper;
import com.turingoal.cms.modules.base.domain.SpecialType;
import com.turingoal.cms.modules.base.domain.form.SpecialTypeForm;
import com.turingoal.cms.modules.base.domain.query.SpecialTypeQuery;
import com.turingoal.cms.modules.base.repository.SpecialTypeDao;
import com.turingoal.cms.modules.base.service.SpecialTypeService;
import com.turingoal.common.annotation.MethodLog;

/**
 * 专题类型 Service
 */
@Service
public class SpecialTypeServiceImpl implements SpecialTypeService {
	@Autowired
	private SpecialTypeDao specialTypeDao;

	/**
	 * 查询全部 专题类型
	 */
	@MethodLog(name = "查询全部专题类型", description = "根据条件查询全部的专题类型，不分页")
	public Page<SpecialType> findAll(final SpecialTypeQuery query) {
		PageHelper.startPage(query.getPage().intValue(), query.getLimit().intValue());
		Page<SpecialType> result = (Page<SpecialType>) specialTypeDao.find(query);
		return result;
	}

	/**
	 * 通过id得到一个 专题类型
	 */
	@MethodLog(name = "通过id得到专题类型", description = "通过id得到一个专题类型")
	public SpecialType get(final String id) {
		return specialTypeDao.get(id);
	}

	/**
	 * 新增 专题类型
	 */
	@MethodLog(name = "新增专题类型", description = "新增一个专题类型，返回id")
	public void add(final SpecialTypeForm form) {
		form.setViewsCount(0);
		Integer priority = specialTypeDao.getMaxPriority(form);
		form.setPriority(priority == null ? 1 : priority + 1);
		form.setCreateDataUsername(SystemHelper.getCurrentUsername());
		specialTypeDao.add(form);
	}

	/**
	 * 修改 专题类型
	 */
	@MethodLog(name = "修改专题类型", description = "修改一个专题类型")
	public int update(final SpecialTypeForm form) {
		form.setUpdateDataUsername(SystemHelper.getCurrentUsername());
		return specialTypeDao.update(form);
	}

	/**
	 * 根据id删除一个 专题类型
	 */
	@MethodLog(name = "删除专题类型", description = "根据id删除一个专题类型")
	public int delete(final String id) {
		specialTypeDao.changePrioritysByDelete(id);
		return specialTypeDao.delete(id);
	}

	/**
	 * 专题类型排序
	 */
	@MethodLog(name = "专题类型排序", description = "修改专题类型排序")
	public int updateOrder(final Integer oldIndex, final Integer newIndex) {
		SpecialTypeForm form = new SpecialTypeForm();
		Integer maxPriority = specialTypeDao.getMaxPriority(form);
		Integer oldPriority = maxPriority - oldIndex; // 原来的优先级 3
		Integer newPriority = maxPriority - newIndex; // 新的优先级 6
		// 修改当前数据的优先级 先临时保存为0
		form.setNewPriority(0);
		form.setOldPriority(oldPriority);
		specialTypeDao.changePriority(form);
		// 上移，中间部分减少优先级
		if (oldIndex > newIndex) {
			form.setStartPriority(oldPriority + 1);
			form.setEndPriority(newPriority);
			specialTypeDao.decreasePrioritys(form);
		} else { // 下移，中间部分增大优先级
			form.setStartPriority(newPriority);
			form.setEndPriority(oldPriority - 1);
			specialTypeDao.increasePrioritys(form);
		}
		// 修改当前数据的优先级
		form.setNewPriority(newPriority);
		form.setOldPriority(0);
		return specialTypeDao.changePriority(form);
	}

}
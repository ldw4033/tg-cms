package com.turingoal.cms.modules.base.web.controller.fore;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring4.SpringTemplateEngine;
import com.turingoal.cms.core.commons.SystemHelper;
import com.turingoal.cms.core.service.SysSensitiveWordService;
import com.turingoal.cms.modules.base.domain.Global;
import com.turingoal.cms.modules.base.domain.Template;
import com.turingoal.cms.modules.base.domain.query.GlobalQuery;
import com.turingoal.cms.modules.base.service.GlobalService;
import com.turingoal.cms.modules.base.service.InfoService;
import com.turingoal.cms.modules.base.service.NodeService;
import com.turingoal.cms.modules.base.service.TemplateService;

/**
 * 模板
 */
@Component
public final class TemplateEngineHelper {
	@Value("${system.nginxPath}")
	private String nginxPath;
	@Autowired
	private SpringTemplateEngine templateEngine;
	@Autowired
	private TemplateService templateService;
	@Autowired
	private GlobalService globalService;
	@Autowired
	private NodeService nodeService;
	@Autowired
	private InfoService infoService;
	@Autowired
	private SysSensitiveWordService sensitiveWordService;

	/**
	 * process
	 */
	public void process(final String templateName, final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (SystemHelper.getGlobal() == null) {
			List<Global> gs = globalService.findAll(new GlobalQuery());
			SystemHelper.setGlobal(gs.get(0));
		}
		Template template = templateService.get(""); // 此处应获取当前启用的模板
		String templateCodeNum = template == null ? "default" : template.getCodeNum();
		String tempName = ("index".equals(templateName) || "search".equals(templateName)) ? templateCodeNum + "/" + templateName : templateName;

		PrintWriter writer = response.getWriter();
		WebContext ctx = new WebContext(request, response, request.getServletContext(), request.getLocale());
		String content = templateEngine.process(tempName, ctx);
		writer.write(content);
		writer.flush();
		return;
	}
}

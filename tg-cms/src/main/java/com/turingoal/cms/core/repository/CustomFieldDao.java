package com.turingoal.cms.core.repository;

import java.util.List;
import java.util.Map;
import com.turingoal.cms.core.domain.CustomField;
import com.turingoal.cms.core.domain.form.CustomFieldForm;
import com.turingoal.cms.core.domain.query.CustomFieldQuery;

/**
 * CustomFieldDao
 */
public interface CustomFieldDao {

	/**
	 * 查询全部 CustomField
	 */
	List<CustomField> find(final CustomFieldQuery query);

	/**
	 * 通过id得到一个 CustomField
	 */
	CustomField get(final String id);

	/**
	 * 通过模型map得到 CustomField
	 */
	List<CustomFieldForm> findByModel(final Map<String, Object> map);

	/**
	 * 新增 CustomField
	 */
	void add(final CustomFieldForm form);

	/**
	 * 修改 CustomField
	 */
	int update(final CustomFieldForm form);

	/**
	 * 根据id删除一个 CustomField
	 */
	int delete(final String id);

	/**
	 * 根据model删除一个 CustomField
	 */
	int deleteByModel(final Map<String, Object> map);

	/**
	 * 根据id删除多个 CustomField
	 */
	int deleteAll(final String ids);

	/**
	 * 修改状态
	 */
	int changeState(final Map<String, Object> map);

	/**
	 * 校验同一模型下的字段名称、字段标签是否重复
	 */
	int validateField(final CustomFieldForm form);

	/**
	 * 获取所有模型字段 带输入值 ownerType 模型类型 ownerId 模型id valueOwnerId 文档/栏目/专题 id
	 */
	List<CustomField> findFieldAndValueByModel(final CustomFieldQuery query);
	
	/**
	 * 根据模型类型、模型id获取最大序号
	 */
	Integer getMaxOrder(final CustomFieldForm form);
	
	/**
	 * 修改序号（加1）
	 */
	int updateOrderAdd(final CustomFieldForm form);
	
	/**
	 * 修改序号（减1）
	 */
	int updateOrderSubtract(final CustomFieldForm form);
	
	/**
	 * 修改CustomField排序（递减）
	 */
	int subtractOrder(final CustomFieldForm form);
	
	/**
	 * 修改CustomField排序（递增）
	 */
	void addOrder(final CustomFieldForm form);
	
	/**
	 * 修改CustomField排序
	 */
	int updateOrder(final CustomFieldForm form);
}
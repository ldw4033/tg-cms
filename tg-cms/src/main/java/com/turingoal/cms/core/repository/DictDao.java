package com.turingoal.cms.core.repository;

import java.util.List;
import java.util.Map;
import com.turingoal.cms.core.domain.Dict;
import com.turingoal.cms.core.domain.form.DictForm;
import com.turingoal.cms.core.domain.query.DictQuery;

/**
 * DictDao
 */
public interface DictDao {

	/**
	 * 根据字典类型查询启用的数据字典
	 */
	List<Dict> findEnabledByType(final String dictType);

	/**
	 * 查询 Dict
	 */
	List<Dict> find(final DictQuery query);

	/**
	 * 通过id得到一个 Dict
	 */
	Dict get(final String id);

	/**
	 * 新增 Dict
	 */
	void add(final DictForm form);

	/**
	 * 修改 Dict
	 */
	int update(final DictForm form);

	/**
	 * 根据id删除一个 Dict
	 */
	int delete(final String id);

	/**
	 * 修改状态
	 */
	int changeState(final Map<String, Object> map);

	/**
	 * 检测数据是否可编辑
	 */
	int checkEditable(final String id);
}
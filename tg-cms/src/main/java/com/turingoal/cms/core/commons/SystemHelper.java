package com.turingoal.cms.core.commons;

import java.util.Collection;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import com.turingoal.cms.core.domain.User;
import com.turingoal.cms.modules.base.domain.Global;
import com.turingoal.common.constants.ConstantSystemValues;

/**
 * 系统帮助工具类
 */
public final class SystemHelper {

	/**
	 * 得到session
	 */
	public static Session getSession() {
		Session session = null;
		Subject subject = SecurityUtils.getSubject();
		if (subject != null) {
			session = subject.getSession(true);
		}
		return session;
	}

	/**
	 * 保存信息到session
	 */
	public static void setSessionAttibute(final String key, final Object value) {
		Session session = getSession();
		if (session != null) {
			session.setAttribute(key, value);
		}
	}

	/**
	 * 从session获取属性
	 */
	public static Object getSessionAttibute(final String key) {
		Object resutlt = null;
		Session session = getSession();
		if (session != null) {
			resutlt = session.getAttribute(key);
		}
		return resutlt;
	}

	/**
	 * 获取当前session的ip
	 */
	public static String getCurrentUserIp() {
		return getSession().getHost();
	}

	/**
	 * 检查用户是已认证
	 */
	public static boolean isAuthenticated() {
		boolean flag = false;
		Subject subject = SecurityUtils.getSubject();
		if (subject != null) {
			flag = subject.isAuthenticated();
		}
		return flag;
	}

	/**
	 * 检查用户是否Remembered
	 */
	public static boolean isRemembered() {
		boolean flag = false;
		Subject subject = SecurityUtils.getSubject();
		if (subject != null) {
			flag = subject.isRemembered();
		}
		return flag;
	}

	/**
	 * 检查用户是有具有某个Permission
	 */
	public static boolean isPermitted(final String permission) {
		boolean flag = false;
		Subject subject = SecurityUtils.getSubject();
		if (subject != null) {
			flag = subject.isPermitted(permission);
		}
		return flag;
	}

	/**
	 * 检查用户是有具有多个Permission
	 */
	public static boolean isPermittedAll(final Collection<Permission> permissions) {
		boolean flag = false;
		Subject subject = SecurityUtils.getSubject();
		if (subject != null) {
			flag = subject.isPermittedAll(permissions);
		}
		return flag;
	}

	/**
	 * 退出系统并清空session
	 */
	public static void logout() {
		Subject subject = SecurityUtils.getSubject();
		if (subject != null) {
			subject.logout();
		}
	}

	/**
	 * 清除缓存认证
	 */
	public static void clearCachedAuthenticationInfo() {
		Subject subject = SecurityUtils.getSubject();
		if (subject != null) {
			RealmSecurityManager securityManager = (RealmSecurityManager) SecurityUtils.getSecurityManager();
			ShiroAuthRealm userRealm = (ShiroAuthRealm) securityManager.getRealms().iterator().next();
			userRealm.clearCachedAuthenticationInfo(subject.getPrincipals());
		}
	}

	/**
	 * 清除缓存授权
	 */
	public void testClearCachedAuthorizationInfo() {
		Subject subject = SecurityUtils.getSubject();
		if (subject != null) {
			RealmSecurityManager securityManager = (RealmSecurityManager) SecurityUtils.getSecurityManager();
			ShiroAuthRealm userRealm = (ShiroAuthRealm) securityManager.getRealms().iterator().next();
			userRealm.clearCachedAuthorizationInfo(subject.getPrincipals());
		}
	}

	/**
	 * 保存当前用户
	 */
	public static void setCurrentUser(final User sysUserinfo) {
		Session session = getSession();
		if (session != null) {
			// 保存用户信息到session
			session.setAttribute(ConstantSystemValues.CURRENT_USER, sysUserinfo);
		}
	}

	/**
	 * 得到当前用户
	 */
	public static User getCurrentUser() {
		User user = null;
		Session session = getSession();
		if (null != session) {
			user = (User) session.getAttribute(ConstantSystemValues.CURRENT_USER);
		}
		return user;
	}

	/**
	 * 得到当前用户id
	 */
	public static String getCurrentUserId() {
		return getCurrentUser().getId();
	}

	/**
	 * 得到当前用户用户名
	 */
	public static String getCurrentUsername() {
		return getCurrentUser().getUsername();
	}

	/**
	 * 得到当前用户真实姓名
	 */
	public static String getCurrentUserRealname() {
		return getCurrentUser().getRealname();
	}

	/**
	 * 设置全局配置
	 */
	public static void setGlobal(final Global global) {
		setSessionAttibute("global", global);
	}

	/**
	 * 得到全局配置
	 */
	public static Global getGlobal() {
		Global resutlt = null;
		Session session = getSession();
		if (session != null) {
			resutlt = (Global) session.getAttribute("global");
		}
		return resutlt;
	}
}

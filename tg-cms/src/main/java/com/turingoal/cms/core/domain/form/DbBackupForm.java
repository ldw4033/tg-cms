package com.turingoal.cms.core.domain.form;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.turingoal.common.bean.BaseFormBean;

/**
 * 数据库备份
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbBackupForm extends BaseFormBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private Date backupDate; // 备份时间
	private String description; // 描述
}
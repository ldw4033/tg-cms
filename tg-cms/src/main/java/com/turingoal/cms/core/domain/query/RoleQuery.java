package com.turingoal.cms.core.domain.query;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.turingoal.common.bean.BaseQueryBean;

/**
 * RoleQuery
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoleQuery extends BaseQueryBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 角色表
	private String roleName; // 角色名称
	private String description; // 描述
	private Integer editable; // 是否可编辑 1可编辑 2不可编辑
	private Integer available; // 是否可用
	private String resourceId;
}
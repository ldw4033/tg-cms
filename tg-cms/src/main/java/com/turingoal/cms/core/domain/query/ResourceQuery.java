package com.turingoal.cms.core.domain.query;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.turingoal.common.bean.BaseQueryBean;

/**
 * ResourceQuery
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResourceQuery extends BaseQueryBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 资源表
	private String resourceName; // 名称
	private String iconCls;
	private Integer type; // 资源类型 1菜单 2按钮 3方法
	private String code; // 权限代码
	private String permission; // 权限标识
	private String permValue;
	private String description; // 描述
	private String parentId; //
	private Integer priority; // 排序
	private Integer editable; // 是否可编辑 1可编辑 2不可编辑
	private Integer available; // 是否可用
	private String roleId;
	private String codeNum;
}
package com.turingoal.cms.core.commons;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.web.filter.mgt.FilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;

/**
 * 和默认的PathMatchingFilterChainResolver区别是，此处得到所有匹配的拦截器链，然后通过调用CustomDefaultFilterChainManager.proxy(originalChain, chainNames)进行合并后代理。
 */
public class ShiroPathMatchingFilterChainResolver extends PathMatchingFilterChainResolver {

	private ShiroDefaultFilterChainManager shiroDefaultFilterChainManager;

	/**
	 * setShiroDefaultFilterChainManager
	 */
	public void setShiroDefaultFilterChainManager(final ShiroDefaultFilterChainManager shiroDefaultFilterChainManagerParm) {
		this.shiroDefaultFilterChainManager = shiroDefaultFilterChainManagerParm;
		setFilterChainManager(shiroDefaultFilterChainManager);
	}

	/**
	 * getChain
	 */
	public FilterChain getChain(final ServletRequest request, final ServletResponse response, final FilterChain originalChain) {
		// 首先获取拦截器链管理器
		FilterChainManager filterChainManager = getFilterChainManager();
		if (!filterChainManager.hasChains()) {
			return null;
		}
		// 获取当前请求的URL（不带上下文）
		String requestURI = getPathWithinApplication(request);
		// 循环拦截器管理器中的拦截器定义（拦截器链的名字就是URL模式）
		List<String> chainNames = new ArrayList<String>();
		for (String pathPattern : filterChainManager.getChainNames()) {
			// 如当前URL匹配拦截器名字（URL模式）
			if (pathMatches(pathPattern, requestURI)) {
				chainNames.add(pathPattern);
			}
		}
		if (chainNames.size() == 0) {
			return null;
		}
		return shiroDefaultFilterChainManager.proxy(originalChain, chainNames);
	}
}

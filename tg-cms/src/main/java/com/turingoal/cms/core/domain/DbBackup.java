package com.turingoal.cms.core.domain;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * DbBackup
 */
@Data
public class DbBackup implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 数据库备份
	private Date backupDate; // 备份时间
	private String description; // 描述
}
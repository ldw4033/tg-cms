package com.turingoal.cms.core.commons;

import java.util.List;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import com.turingoal.cms.core.domain.User;
import com.turingoal.cms.core.repository.UserDao;
import com.turingoal.cms.core.service.ResourceService;
import com.turingoal.common.constants.ConstantAvailableValue;
import com.turingoal.common.util.io.PropsUtil;

/**
 * Shiro 继承 AuthorizingRealm接口
 */
public class ShiroAuthRealm extends AuthorizingRealm {
	private static String applicationSalt = PropsUtil.getValue("shiro.applicationSalt", "application.properties");
	@Autowired
	private UserDao userDao;
	@Autowired
	private ResourceService resourceService;

	/**
	 * doGetAuthenticationInfo是为认证用户的正确性，如登录认证
	 */
	@Override
	protected final AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token) {
		String username = (String) token.getPrincipal(); // 用户名
		User user = userDao.findByUsername(username);
		if (user == null) {
			throw new UnknownAccountException(); // 没找到帐号 // 其它待实现
		} else if (user.getAvailable() != ConstantAvailableValue.AVAILABLE_INT) {
			throw new DisabledAccountException("账号已停用,登录失败！");
		}
		// } catch (UnknownAccountException uae) {
		// result.put("msg", "该用户名不存在！");
		// } catch (ExcessiveAttemptsException eae) {
		// result.put("msg", "密码错误次数太多！请确认用户名和密码！");
		// result.put("showCaptchaCode", true); // 需要输入验证码
		// } catch (IncorrectCredentialsException ice) {
		// result.put("msg", "认证信息不正确");
		// } catch (LockedAccountException lae) {
		// result.put("msg", "密码错误次数太多，账号已锁定，请稍后再试！");
		// } catch (DisabledAccountException lae) {
		// result.put("msg", "你的账户已被禁用,请联系管理员开通！");
		// } catch (AuthenticationException ae) {
		// result.put("msg", "认证失败，请重新登录！");
		// }
		// 通过当前用户和用户密码创建一个 SimpleAuthenticationInfo 然后去匹配密码是否正确
		return new SimpleAuthenticationInfo(username, user.getUserPass(), ByteSource.Util.bytes(applicationSalt + user.getUserSalt() + username), getName());
	}

	/**
	 * 为用户加Role、permission.暂时不使用role
	 */
	@Override
	protected final AuthorizationInfo doGetAuthorizationInfo(final PrincipalCollection principals) {
		String username = (String) principals.getPrimaryPrincipal(); // prin.asList().get(0).toString()
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

		@SuppressWarnings("unchecked")
		List<String> auths = (List<String>) SystemHelper.getSessionAttibute("auths");
		if (auths == null) {
			auths = resourceService.findPermissionsEnabledByUser(username);
		}
		// 将用户拥有的permission放入到SimpleAuthorizationInfo对象中
		info.addStringPermissions(auths);
		// authorizationInfo.setRoles(userService.findRoles(username)); 暂时不用
		return info;
	}

	@Override
	public void clearCachedAuthorizationInfo(final PrincipalCollection principals) {
		super.clearCachedAuthorizationInfo(principals);
	}

	@Override
	public void clearCachedAuthenticationInfo(final PrincipalCollection principals) {
		super.clearCachedAuthenticationInfo(principals);
	}

	@Override
	public void clearCache(final PrincipalCollection principals) {
		super.clearCache(principals);
	}

	/**
	 * clearAllCachedAuthorizationInfo
	 */
	public void clearAllCachedAuthorizationInfo() {
		getAuthorizationCache().clear();
	}

	/**
	 * clearAllCachedAuthenticationInfo
	 */
	public void clearAllCachedAuthenticationInfo() {
		getAuthenticationCache().clear();
	}

	/**
	 * clearAllCache
	 */
	public void clearAllCache() {
		clearAllCachedAuthenticationInfo();
		clearAllCachedAuthorizationInfo();
	}
}

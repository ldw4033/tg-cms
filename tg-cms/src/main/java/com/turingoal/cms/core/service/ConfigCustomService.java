package com.turingoal.cms.core.service;

import java.util.Map;

/**
 * 个性化定制
 */
public interface ConfigCustomService {

	/**
	 * 得到登录页面信息
	 */
	Map<String, Object> getLoginInfo();

	/**
	 * 得到主页面基本信息
	 */
	Map<String, Object> getIndexInfo();
}

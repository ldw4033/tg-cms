package com.turingoal.cms.core.commons;

import java.util.concurrent.atomic.AtomicInteger;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;

/**
 * 密码重试次数限制RetryLimitHashedCredentialsMatcher 最大出错次数利用缓存实现，验证码次数用session实现
 */
public class ShiroRetryLimitHashedCredentialsMatcher extends HashedCredentialsMatcher {
	private Cache<String, AtomicInteger> passwordRetryCache;
	private boolean captchaEbabled = true; // 是否开启验证码支持
	private int showCaptchaRetryCount = 3; // 验证出错次数显示验证码
	private int maxRetryCount = 5; // 密码最大重试次数

	public void setCaptchaEbabled(final boolean captchaEbabledParm) {
		this.captchaEbabled = captchaEbabledParm;
	}

	public void setShowCaptchaRetryCount(final int showCaptchaRetryCountParm) {
		this.showCaptchaRetryCount = showCaptchaRetryCountParm;
	}

	public ShiroRetryLimitHashedCredentialsMatcher(final CacheManager cacheManager) {
		passwordRetryCache = cacheManager.getCache("shiro-passwordRetryCache");
	}

	public void setMaxRetryCount(final int maxRetryCountParm) {
		this.maxRetryCount = maxRetryCountParm;
	}

	@Override
	public boolean doCredentialsMatch(final AuthenticationToken token, final AuthenticationInfo info) {
		Session session = SystemHelper.getSession();
		String username = (String) token.getPrincipal();
		// 缓存里的错误次数
		AtomicInteger retryCount = passwordRetryCache.get(username);
		if (retryCount == null) {
			retryCount = new AtomicInteger(0);
			passwordRetryCache.put(username, retryCount);
		}
		int count = retryCount.incrementAndGet();
		if (count > maxRetryCount) { // 是否超过最大次数
			throw new LockedAccountException();
		}
		// session里的错误次数 ，用于显示验证码
		AtomicInteger sessionRetryCount = (AtomicInteger) session.getAttribute("sessionRetryCount");
		if (sessionRetryCount == null) {
			sessionRetryCount = new AtomicInteger(0);
			session.setAttribute("sessionRetryCount", sessionRetryCount);
		}
		int sessionRetryErrorCount = sessionRetryCount.incrementAndGet();
		boolean matches = super.doCredentialsMatch(token, info);
		if (matches) {
			passwordRetryCache.remove(username);
			session.setAttribute("showCaptchaCode", false);
		} else if (captchaEbabled && sessionRetryErrorCount >= showCaptchaRetryCount) { // 判断是否显示验证码
			session.setAttribute("showCaptchaCode", true); // 需要输入验证码
			throw new ExcessiveAttemptsException();
		}
		return matches;
	}
}

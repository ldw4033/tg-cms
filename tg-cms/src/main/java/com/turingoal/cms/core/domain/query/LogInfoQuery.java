package com.turingoal.cms.core.domain.query;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.turingoal.common.bean.BaseQueryBean;

/**
 * LoginfoQuery
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class LogInfoQuery extends BaseQueryBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 操作日志
	private String logType; // 类型(操作日志,登录日志)
	private String message; //
	private String exception; //
	private String ipAddress; // IP
	private Date eventDate; // 时间
	private String username; //
	private Date startTime; // 开始时间
	private Date endTime; // 结束时间
}
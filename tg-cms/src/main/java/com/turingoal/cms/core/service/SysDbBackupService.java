package com.turingoal.cms.core.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import com.turingoal.cms.core.domain.DbBackup;
import com.turingoal.cms.core.domain.form.DbBackupForm;

/**
 * 数据库备份Service
 */
public interface SysDbBackupService {

	/**
	 * 获得Schema
	 */
	String getSchema();

	/**
	 * 获得所有表名
	 */
	List<String> getTables(final String schema);

	/**
	 * 获得TableDDL
	 */
	String getTableDDL(final String schema, final String table);

	/**
	 * getDataByTable
	 */
	List<Map<String, Object>> getDataByTable(final String schema, final String table);

	/**
	 * writeTableData
	 */
	void writeTableData(String schema, final String table, final BufferedWriter writer) throws IOException;

	/**
	 * backup
	 */
	void backup(File dir, boolean dataOnly) throws IOException;

	/**
	 * 查询全部 DbBackup
	 */
	List<DbBackup> findAll();

	/**
	 * 新增 DbBackup
	 */
	void add(final DbBackupForm form) throws IOException;

	/**
	 * 根据id删除一个 DbBackup
	 */
	int delete(final String id);
}

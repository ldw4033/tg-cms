package com.turingoal.cms.core.web.controller.admin;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.turingoal.cms.core.domain.form.DbBackupForm;
import com.turingoal.cms.core.domain.query.DbBackupQuery;
import com.turingoal.cms.core.service.SysDbBackupService;
import com.turingoal.common.bean.JsonResultBean;
import com.turingoal.common.bean.PageGridBean;
import com.turingoal.common.constants.ConstantDateFormatTypes;
import com.turingoal.common.exception.BusinessException;
import com.turingoal.common.util.spring.SpringBindingResultWrapper;
import com.turingoal.common.util.validator.ValidGroupAdd;

/**
 * 数据库备份还原Controller
 */
@Controller
@RequestMapping("/c/dbBackup")
public class SysDbBackupController {
	private static final String LIST_PAGE = "core/system/dbBackup/list";
	private static final String ADD_PAGE = "core/system/dbBackup/add";
	@Autowired
	private SysDbBackupService sysDbBackupService;

	/**
	 * 数据库备份还原页面
	 */
	@RequestMapping(value = "/list.gsp", method = RequestMethod.GET)
	public String listPage() throws BusinessException {
		return LIST_PAGE;
	}

	/**
	 * 查询全部 DbBackup
	 */
	@RequestMapping(value = "/list.gsp", method = RequestMethod.POST)
	@ResponseBody
	public Object list(final DbBackupQuery query) throws BusinessException {
		return new PageGridBean(query, sysDbBackupService.findAll());
	}

	/**
	 * 新增数据库备份
	 */
	@RequestMapping(value = "/add.gsp", method = RequestMethod.GET)
	public String addPage() {
		return ADD_PAGE;
	}

	/**
	 * 新增 DbBackup
	 */
	@RequestMapping(value = "/add.gsp", method = RequestMethod.POST)
	@ResponseBody
	public final JsonResultBean add(@Validated({ ValidGroupAdd.class }) @ModelAttribute("form") final DbBackupForm form, final BindingResult bindingResult) throws BusinessException, IOException {
		// 数据校验
		if (bindingResult.hasErrors()) {
			String errorMsg = SpringBindingResultWrapper.warpErrors(bindingResult);
			return new JsonResultBean(JsonResultBean.FAULT, errorMsg);
		} else {
			sysDbBackupService.add(form);
			return new JsonResultBean(JsonResultBean.SUCCESS);
		}
	}

	/**
	 * 根据id删除数据库备份
	 */
	@RequestMapping(value = "/delete_{id}.gsp", method = RequestMethod.POST)
	@ResponseBody
	public final JsonResultBean delete(@PathVariable("id") final String id) throws BusinessException {
		sysDbBackupService.delete(id);
		return new JsonResultBean(JsonResultBean.SUCCESS);
	}

	/**
	 * 将form表单里面的String Date转换成Date型，字符串去掉空白
	 */
	@InitBinder
	protected final void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(ConstantDateFormatTypes.YYYY_MM_DD), true));
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
}
package com.turingoal.cms.core.service.impl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.turingoal.cms.core.service.ConfigSysService;
import com.turingoal.common.util.io.PropsUtil;

/**
 * 系统配置Service
 */
@Service
public class ConfigSysServiceImpl implements ConfigSysService {

	public static final String PROP_FILE_NAME = "application.properties";

	/** 读取配置文件的值，分号后面为没有此配置项时的默认值 */
	@Value("${login.isCheckCodeShow:true}")
	private boolean isCheckCodeShow;
	@Value("${login.isErrorsLock:true}")
	private boolean isErrorsLock;
	@Value("${login.checkShowNum:3}")
	private int checkShowNum;
	@Value("${login.lockNum:5}")
	private int lockNum;
	@Value("${login.errorBreakTime:30}")
	private int errorBreakTime;
	@Value("${login.lockTime:120}")
	private int lockTime;

	/**
	 * 取配置文件里的值
	 */
	public String getPropValue(final String key) {
		return PropsUtil.getValue(key, "../config/" + PROP_FILE_NAME);
	}

	/**
	 * 设置配置文件里的值
	 */
	public void setPropValue(final String key, final Object value) {
		PropsUtil.setValue(key, value.toString(), "../config/" + PROP_FILE_NAME);
	}

	/**
	 * 得到系统全局配置
	 */
	public Map<String, Object> getSystemConfig() {
		Map<String, Object> config = new HashMap<String, Object>();
		config.put("systemPath", getPropValue("systemPath")); // 是否显示验证码
		config.put("systemURL", getPropValue("systemURL")); // 是否锁定用户
		config.put("imagePath", getPropValue("imagePath")); // 显示验证码错误次数
		config.put("uploadPath", getPropValue("uploadPath")); // 锁定用户错误次数
		config.put("uploadURL", getPropValue("uploadURL")); // 错误失效时间
		return config;
	}

	/**
	 * 得到用户登录配置
	 */
	public Map<String, Object> getLoginConfig() {
		Map<String, Object> config = new HashMap<String, Object>();
		config.put("isCheckCodeShow", getPropValue("isCheckCodeShow")); // 是否显示验证码
		config.put("isErrorsLock", getPropValue("isErrorsLock")); // 是否锁定用户
		config.put("checkShowNum", getPropValue("checkShowNum")); // 显示验证码错误次数
		config.put("lockNum", getPropValue("lockNum")); // 锁定用户错误次数
		config.put("errorBreakTime", getPropValue("errorBreakTime")); // 错误失效时间
		config.put("lockTime", getPropValue("lockTime")); // 用户锁定时间
		return config;
	}

	/**
	 * 修改用户登录配置
	 */
	public boolean updateLoginConfig(final Map<String, Object> config) {
		setPropValue("isCheckCodeShow", config.get("isCheckCodeShow")); // 是否显示验证码
		setPropValue("isErrorsLock", config.get("isErrorsLock")); // 是否锁定用户
		setPropValue("checkShowNum", config.get("checkShowNum")); // 显示验证码错误次数
		setPropValue("lockNum", config.get("lockNum")); // 锁定用户错误次数
		setPropValue("errorBreakTime", config.get("errorBreakTime")); // 错误失效时间
		setPropValue("lockTime", config.get("lockTime")); // 用户锁定时间
		return true;
	}

	/**
	 * 是否显示验证码
	 */
	public boolean isCheckCodeShow() {
		return isCheckCodeShow;
	}

	/**
	 * 显示验证码的最小次数
	 */
	public int getCheckCodeShowNum() {
		return checkShowNum;
	}

	/**
	 * 是否出错超过次数锁定
	 */
	public boolean isErrosLock() {
		return isErrorsLock;
	}

	/**
	 * 出错次数账号锁定
	 */
	public int getLockNum() {
		return lockNum;
	}

	/**
	 * 密码错误失效时间
	 */
	public int getErrorBreakTime() {
		return errorBreakTime;
	}

	/**
	 * 锁定时间
	 */
	public int getLockTime() {
		return lockTime;
	}

}

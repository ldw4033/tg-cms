package com.turingoal.cms.core.commons;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import org.apache.shiro.config.Ini;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.util.Nameable;
import org.apache.shiro.util.StringUtils;
import org.apache.shiro.web.config.IniFilterChainResolverFactory;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.NamedFilterList;
import org.apache.shiro.web.filter.mgt.SimpleNamedFilterList;

/**
 * ShiroDefaultFilterChainManager
 */
public class ShiroDefaultFilterChainManager extends DefaultFilterChainManager {
	private Map<String, String> filterChainDefinitionMap = null; // 用于存储如ShiroFilterFactoryBean在配置文件中配置的拦截器链定义，即可以认为是默认的静态拦截器链；会自动与数据库中加载的合并；
	private String loginUrl; // 登录地址
	private String successUrl; // 登录成功后默认跳转地址
	private String unauthorizedUrl; // 未授权跳转地址

	/**
	 * 调用其构造器时，会自动注册默认的拦截器；
	 */
	public ShiroDefaultFilterChainManager() {
		setFilters(new LinkedHashMap<String, Filter>());
		setFilterChains(new LinkedHashMap<String, NamedFilterList>());
		addDefaultFilters(false);
	}

	public Map<String, String> getFilterChainDefinitionMap() {
		return filterChainDefinitionMap;
	}

	public void setFilterChainDefinitionMap(final Map<String, String> filterChainDefinitionMapParm) {
		this.filterChainDefinitionMap = filterChainDefinitionMapParm;
	}

	/**
	 * 注册我们自定义的拦截器；如ShiroFilterFactoryBean的filters属性；
	 */
	public void setCustomFilters(final Map<String, Filter> customFilters) {
		for (Map.Entry<String, Filter> entry : customFilters.entrySet()) {
			addFilter(entry.getKey(), entry.getValue(), false);
		}
	}

	/**
	 * 解析配置文件中传入的字符串拦截器链配置，解析为相应的拦截器链；
	 */
	public void setDefaultFilterChainDefinitions(final String definitions) {
		Ini ini = new Ini();
		ini.load(definitions);
		Ini.Section section = ini.getSection(IniFilterChainResolverFactory.URLS);
		if (CollectionUtils.isEmpty(section)) {
			section = ini.getSection(Ini.DEFAULT_SECTION_NAME);
		}
		setFilterChainDefinitionMap(section);
	}

	/**
	 * 初始化方法，Spring容器启动时会调用，首先其会自动给相应的拦截器设置如loginUrl、successUrl、unauthorizedUrl；其次根据filterChainDefinitionMap构建默认的拦截器链；
	 */
	@PostConstruct
	public void init() {
		// Apply the acquired and/or configured filters:
		Map<String, Filter> filters = getFilters();
		if (!CollectionUtils.isEmpty(filters)) {
			for (Map.Entry<String, Filter> entry : filters.entrySet()) {
				String name = entry.getKey();
				Filter filter = entry.getValue();
				applyGlobalPropertiesIfNecessary(filter);
				if (filter instanceof Nameable) {
					((Nameable) filter).setName(name);
				}
			}
		}
		// build up the chains:
		Map<String, String> chains = getFilterChainDefinitionMap();
		if (!CollectionUtils.isEmpty(chains)) {
			for (Map.Entry<String, String> entry : chains.entrySet()) {
				String url = entry.getKey();
				String chainDefinition = entry.getValue();
				createChain(url, chainDefinition);
			}
		}
	}

	/**
	 * 此处我们忽略实现initFilter，因为交给spring管理了，所以Filter的相关配置会在Spring配置中完成；
	 */
	@Override
	protected void initFilter(final Filter filter) {
		// ignore
	}

	/**
	 * 组合多个拦截器链为一个生成一个新的FilterChain代理。
	 */
	public FilterChain proxy(final FilterChain original, final List<String> chainNames) {
		NamedFilterList configured = new SimpleNamedFilterList(chainNames.toString());
		for (String chainName : chainNames) {
			configured.addAll(getChain(chainName));
		}
		return configured.proxy(original);
	}

	/**
	 * applyLoginUrlIfNecessary
	 */
	private void applyLoginUrlIfNecessary(final Filter filter) {
		String loginURL = getLoginUrl();
		if (StringUtils.hasText(loginURL) && (filter instanceof AccessControlFilter)) {
			AccessControlFilter acFilter = (AccessControlFilter) filter;
			// only apply the login url if they haven't explicitly configured one already:
			String existingLoginUrl = acFilter.getLoginUrl();
			if (AccessControlFilter.DEFAULT_LOGIN_URL.equals(existingLoginUrl)) {
				acFilter.setLoginUrl(loginURL);
			}
		}
	}

	/**
	 * applySuccessUrlIfNecessary
	 */
	private void applySuccessUrlIfNecessary(final Filter filter) {
		String successURL = getSuccessUrl();
		if (StringUtils.hasText(successURL) && (filter instanceof AuthenticationFilter)) {
			AuthenticationFilter authcFilter = (AuthenticationFilter) filter;
			// only apply the successUrl if they haven't explicitly configured one already:
			String existingSuccessUrl = authcFilter.getSuccessUrl();
			if (AuthenticationFilter.DEFAULT_SUCCESS_URL.equals(existingSuccessUrl)) {
				authcFilter.setSuccessUrl(successURL);
			}
		}
	}

	/**
	 * applyUnauthorizedUrlIfNecessary
	 */
	private void applyUnauthorizedUrlIfNecessary(final Filter filter) {
		String unauthorizedURL = getUnauthorizedUrl();
		if (StringUtils.hasText(unauthorizedURL) && (filter instanceof AuthorizationFilter)) {
			AuthorizationFilter authzFilter = (AuthorizationFilter) filter;
			// only apply the unauthorizedUrl if they haven't explicitly configured one already:
			String existingUnauthorizedUrl = authzFilter.getUnauthorizedUrl();
			if (existingUnauthorizedUrl == null) {
				authzFilter.setUnauthorizedUrl(unauthorizedURL);
			}
		}
	}

	/**
	 * applyUnauthorizedUrlIfNecessary
	 */
	private void applyGlobalPropertiesIfNecessary(final Filter filter) {
		applyLoginUrlIfNecessary(filter);
		applySuccessUrlIfNecessary(filter);
		applyUnauthorizedUrlIfNecessary(filter);
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(final String loginUrlParm) {
		this.loginUrl = loginUrlParm;
	}

	public String getSuccessUrl() {
		return successUrl;
	}

	public void setSuccessUrl(final String successUrlParm) {
		this.successUrl = successUrlParm;
	}

	public String getUnauthorizedUrl() {
		return unauthorizedUrl;
	}

	public void setUnauthorizedUrl(final String unauthorizedUrlParm) {
		this.unauthorizedUrl = unauthorizedUrlParm;
	}

}

package com.turingoal.cms.core.domain;

import java.io.Serializable;
import lombok.Data;

/**
 * Role
 */
@Data
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 角色表
	private String roleName; // 角色名称
	private String description; // 描述
	private Integer priority; // 优先级
	private Integer editable; // 是否可编辑 1可编辑 2不可编辑
	private Integer available; // 是否可用
	private String userRoleId;
}
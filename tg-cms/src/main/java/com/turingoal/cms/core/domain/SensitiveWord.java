package com.turingoal.cms.core.domain;

import java.io.Serializable;
import lombok.Data;

/**
 * 敏感词
 */
@Data
public class SensitiveWord implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 敏感词
	private String word; // word
	private String replacement; // replacement
	private Integer available; // 是否可用
}